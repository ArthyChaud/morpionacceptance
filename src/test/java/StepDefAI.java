import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import morpion.Game;

public class StepDefAI {
  Game game;

  @Given("^The middle is free$")
  public void theMiddleIsFree() {
    game = new Game();
    game.resetGrid();
  }

  @When("^The AI play$")
  public void theAIPlay() {
    game.getPlayer2().makeAMove(game);
  }

  @Then("^AI take place at the middle$")
  public void AITakePlaceAtTheMiddle() {
    if (game.getGrid()[1][1] == game.getPlayer2().getSymbol()) System.out.println("AI take place at the middle");
  }

  @Given("^The case \\((\\d+),(\\d+)\\) is taken by the player$")
  public void theCaseIsTakenByThePlayer(int arg0, int arg1) {
    if (game == null) {
      game = new Game();
      game.resetGrid();
    }
    game.getGrid()[arg1][arg0] = game.getPlayer1().getSymbol();
    System.out.format("The case (%d, %d) is taken by the player", arg0, arg1);
  }

  @Then("^AI take place at the case \\((\\d+),(\\d+)\\)$")
  public void AITakePlaceAtTheCase(int arg0, int arg1) {
    if (game == null) {
      game = new Game();
      game.resetGrid();
    }
    System.out.println(game.getGrid()[arg1][arg0]);
    if (game.getGrid()[arg1][arg0] == game.getPlayer2().getSymbol())
      System.out.printf("The case (%d, %d) is taken by the AI", arg0, arg1);
  }

  @Given("^The case \\((\\d+),(\\d+)\\) is taken by the AI$")
  public void theCaseIsTakenByTheAI(int arg0, int arg1) {
    if (game == null) {
      game = new Game();
      game.resetGrid();
    }
    System.out.println(game.getGrid()[arg1][arg0]);
    if (game.getGrid()[arg1][arg0] == game.getPlayer2().getSymbol())
      System.out.format("The case (%d, %d) is taken by the AI", arg0, arg1);
  }
}
