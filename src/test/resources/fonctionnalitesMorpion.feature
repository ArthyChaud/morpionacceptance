Feature: AI
  Scenario: Play middle
    Given The middle is free
    When The AI play
    Then AI take place at the middle

  Scenario: Counter player on column
    Given The case (0,0) is taken by the player
    And The case (0,1) is taken by the player
    When The AI play
    Then AI take place at the case (0,2)

  Scenario: Counter player in line
    Given The case (0,0) is taken by the player
    And The case (1,0) is taken by the player
    When The AI play
    Then AI take place at the case (2,0)

  Scenario: Counter player in diagonal
    Given The case (0,0) is taken by the player
    And The case (1,1) is taken by the player
    When The AI play
    Then AI take place at the case (2,2)

  Scenario:  Take a column
    Given The case (0,0) is taken by the AI
    And The case (0,1) is taken by the AI
    When The AI play
    Then AI take place at the case (0,2)

  Scenario: Take a line
    Given The case (0,0) is taken by the AI
    And The case (1,0) is taken by the AI
    When The AI play
    Then AI take place at the case (2,0)

  Scenario: Take a diagonal
    Given The case (0,0) is taken by the AI
    And The case (1,1) is taken by the AI
    When The AI play
    Then AI take place at the case (2,2)
